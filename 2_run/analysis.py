import numpy as np
import matplotlib.pyplot as plt
import datetime as dt


# Step 1: Parse the external temperature file
external_temps = {}
with open('second_run/temperature_2023-09-06_14-59-14.log', 'r', encoding='utf-8') as f:
  for line in f:
    if line.strip():  # Check if line is not empty
      parts = line.split('\t')
      timestamp = dt.datetime.strptime(parts[0], '%Y-%m-%d %H:%M:%S')
      for part in parts[1:]:
        if ':' in part:
          module, temp = part.split(':')
          module = module.strip()
          temp = float(temp.strip())
          if module not in external_temps:
            external_temps[module] = []
          external_temps[module].append((timestamp, temp))

# Step 2: Adjust the timestamps
for module, data in external_temps.items():
  adjusted_data = []
  for timestamp, temp in data:
    adjusted_data.append((timestamp + dt.timedelta(hours=2), temp))
  external_temps[module] = adjusted_data



def remove_outliers(data):
  Q1 = np.percentile(data, 25)
  Q3 = np.percentile(data, 75)
  IQR = Q3 - Q1
  lower_bound = Q1 - 1.5 * IQR
  upper_bound = Q3 + 1.5 * IQR
  return data[(data >= lower_bound) & (data <= upper_bound)]


# Step 0: Load Data
with open('second_run/tstCDC_2023_09_06-17_01_11.txt', 'r', encoding="utf-8" ) as f:
  data_str = f.read()

data = np.array([list(map(float, line.split())) for line in data_str.split('\n') if line])
V1, V2, R = data[:,0], data[:,1], data[:,2]

V1_offset = (V1[::2] + V1[1::2]) / 2  
V2_offset = (V2[::2] + V2[1::2]) / 2

V1_no_offset = V1 - np.repeat(V1_offset, 2)
V2_no_offset = V2 - np.repeat(V2_offset, 2)

V_final = np.absolute( (V1_no_offset + V2_no_offset) / 2) 


# Step 2: Convert Resistance to Temperature (simplified for example)
Temperature = (R - 100) / 0.385  # A simple linear relationship for PT100 between 0°C and 100°C



# Compute the cross-correlation
cross_corr = np.correlate(V_final, Temperature, mode='full')

# Create an array for lags
lags = np.arange(-len(V_final) + 1, len(V_final))

# Convert V_final to ppm
V_final_avg = np.mean(V_final)
V_final_ppm = (V_final - V_final_avg) * 1e6 / V_final_avg


V_final_ppm_filtered = remove_outliers(V_final_ppm)

window_size = 100
window = np.ones(window_size) / window_size
V_final_ppm_filtered_avg = np.convolve(V_final_ppm_filtered, window, mode='valid')



# Step 3: Align the data
start_time = dt.datetime(2023, 9, 6, 17, 1, 11)  # Start time from your data
aligned_temps = {module: [] for module in external_temps}
for module, data in external_temps.items():
  current_idx = 0
  for second in range(len(V1)):  # Assuming V1 is the array you provided in the code snippet
    current_time = start_time + dt.timedelta(seconds=second)
    while current_idx < len(data) - 1 and data[current_idx + 1][0] <= current_time:
      current_idx += 1
    aligned_temps[module].append(data[current_idx][1])



# Step 4: Plotting
plt.figure(figsize=(36, 12))

plt.subplot(3, 3, 1)
plt.plot(V_final)
plt.title('V_final Plot')
plt.xlabel('Sample Number')
plt.ylabel('Value')

plt.subplot(3, 3, 4)


# plot also V_final_ppm_filtered_avg

sigma = np.std(V_final_ppm_filtered_avg)
mean = np.mean(V_final_ppm_filtered_avg)


# 1 Sigma bands
plt.axhspan(mean - sigma, mean + sigma, color='red', alpha=0.1, label='1 Sigma') 
plt.text(0, mean + sigma, f'+1σ ({sigma:.2f} ppm)', color='r', horizontalalignment='left')
# plt.text(0, mean - sigma, f'-1σ ({sigma:.2f} ppm)', color='r', horizontalalignment='left')

# 2 Sigma bands
plt.axhspan(mean - 2*sigma, mean + 2*sigma, color='blue', alpha=0.05, label='2 Sigma')  # Use a lesser alpha to differentiate from 1 sigma
plt.text(0, mean + 2*sigma, f'+2σ ({2*sigma:.2f} ppm)',  color='b', horizontalalignment='left')
# plt.text(0, mean - 2*sigma, f'-2σ ({2*sigma:.2f} ppm)',  color='b', horizontalalignment='left')


plt.plot(V_final_ppm_filtered_avg, label='V_final_ppm_filtered_avg')

ticks_interval = 0.05  # spacing in ppm
max_val = np.ceil(np.max(np.abs(V_final_ppm_filtered_avg)) / ticks_interval) * ticks_interval
y_ticks = np.arange(-max_val, max_val + ticks_interval, ticks_interval)

# Set y-tick intervals
plt.yticks(y_ticks)


# Display grid
plt.grid(axis='y', linestyle='--', linewidth=0.5)

plt.title('Filtered V_final_ppm_filtered_avg Plot')
plt.xlabel('Sample Number')
plt.ylabel('Value in ppm')

plt.title('Filtered V_final_ppm_filtered_avg Plot')
plt.xlabel('Sample Number')
plt.ylabel('Value in ppm')

plt.subplot(3, 3, 5)

ax1 = plt.gca()  # Gets the current active axis
ax1.plot(V_final_ppm_filtered_avg, label='V_final_ppm_filtered')
ax1.set_xlabel('Sample Number')
ax1.set_ylabel('Value in ppm')
ax1.tick_params('y')
plt.title('V_final_ppm_filtered_avg vs Controller Temp')

# Create a second y-axis to plot the controller temperature
if 'controller' in aligned_temps:
  ax2 = ax1.twinx()  # Create a twin y-axis sharing the x-axis
  # use the matlab orange for this plot 
  ax2.plot(aligned_temps['controller'], label='Controller Temp', color='tab:orange')
  ax2.set_ylabel('Temperature (°C)', color='tab:orange')
  ax2.tick_params('y', colors='tab:orange')


# Plot the temperature of the resistor
plt.subplot(3, 3, 2)
ax_temp = plt.gca()
ax_temp.plot(Temperature, label='Resistor Temp', color='k', linewidth=2)
ax_temp.set_title('Temperature of the resistor and Controller')
ax_temp.set_xlabel('Sample Number')
ax_temp.set_ylabel('Resistor Temperature (°C)')
ax_temp.legend(loc="upper left")

if 'controller' in aligned_temps:
  ax_controller = ax_temp.twinx()  # Create a twin y-axis sharing the x-axis
  ax_controller.plot(aligned_temps['controller'], label='Controller Temp', color='tab:orange')
  ax_controller.set_ylabel('Controller Temperature (°C)', color='tab:orange')
  ax_controller.tick_params('y', colors='tab:orange')
  ax_controller.legend(loc="upper right")
# Compute the cross-correlation
cross_corr = np.correlate(Temperature - np.mean(Temperature), aligned_temps['controller'] - np.mean(aligned_temps['controller']), "full")

# Find the lag at which the maximum correlation occurs
lag_of_max_corr = np.argmax(cross_corr) - len(Temperature) + 1


# Plot the temperatures for all the modules and the Temperature
plt.subplot(3, 3, (7,8))
# Calculate the mean value for Resistor Temperature as its set point
Temperature_normalized = Temperature - np.mean(Temperature)


# Plotting the normalized temperatures for the modules
for module, temps in aligned_temps.items():
  temps_normalized = np.array(temps) - np.mean(temps)
  plt.plot(temps_normalized, label=module)

# Plotting the normalized resistor temperature
plt.plot(Temperature_normalized, label='Resistor Temp', color='k', linewidth=2)

# force the y scale to be between -2 and 0.5
plt.ylim(-2, 0.5)

plt.xlabel('Sample Number')
plt.ylabel('Temperature Variation (°C)')
plt.legend()
plt.title('Temperature Variations for all Modules and Resistor Temp')

# ...

# Plotting for V_final_ppm_filtered and Temperature
plt.subplot(3, 3, 6)
cross_corr_V_final = np.correlate(Temperature - np.mean(Temperature), 
                                 V_final_ppm_filtered - np.mean(V_final_ppm_filtered), 
                                 "full")
lags_V_final = np.arange(-len(V_final_ppm_filtered) + 1, len(Temperature))


search_range = 1000  # The range to search for the maximum value in the cross-correlation
# Find the index of the maximum value in the cross-correlation within the restricted range
restrict_range_indices = np.where((lags_V_final >= -search_range) & (lags_V_final <= search_range))
max_index_V_final_restricted = np.argmax(cross_corr_V_final[restrict_range_indices])
max_lag_V_final_restricted = lags_V_final[restrict_range_indices][max_index_V_final_restricted]

plt.plot(lags_V_final, cross_corr_V_final)
plt.scatter(max_lag_V_final_restricted, cross_corr_V_final[restrict_range_indices][max_index_V_final_restricted], color='red')  # highlight the max value with a red dot
plt.annotate(f'Lag = {max_lag_V_final_restricted} s',
             (max_lag_V_final_restricted, cross_corr_V_final[restrict_range_indices][max_index_V_final_restricted]),
             xytext=(10,-10), textcoords='offset points')
plt.title('Cross-Correlation between V_final_ppm_filtered and burden resistor temperature')
plt.xlabel('Lag')
plt.ylabel('Cross-correlation')

# Plotting for Temperature and Controller Temp
plt.subplot(3, 3, 3)
cross_corr_temp_controller = np.correlate(Temperature - np.mean(Temperature), 
                                          aligned_temps['controller'] - np.mean(aligned_temps['controller']),
                                          "full")
lags_temp_controller = np.arange(-len(Temperature) + 1, len(aligned_temps['controller']))

# Find the index of the maximum value in the cross-correlation
max_index_temp_controller = np.argmax(cross_corr_temp_controller)
max_lag_temp_controller = lags_temp_controller[max_index_temp_controller]

plt.plot(lags_temp_controller, cross_corr_temp_controller)
plt.scatter(max_lag_temp_controller, cross_corr_temp_controller[max_index_temp_controller], color='red')  # highlight the max value with a red dot
plt.annotate(f'Lag = {max_lag_temp_controller} s',
             (max_lag_temp_controller, cross_corr_temp_controller[max_index_temp_controller]),
             xytext=(10,-10), textcoords='offset points')
plt.title('Cross-Correlation between burden resistor temperature and Controller Temp')
plt.xlabel('Lag')
plt.ylabel('Cross-correlation')

# In the last plot, plot V1 and V2 filtered with a 100 sample window and with the average removed
plt.subplot(3, 3, 9)
V1_no_avg = V1 - np.mean(V1)
V2_no_avg = V2 - np.mean(V2)
V1_filtered = np.convolve(V1_no_avg, window, mode='valid')
V2_filtered = np.convolve(V2_no_avg, window, mode='valid')
plt.plot(V1_filtered, label='V1')
plt.plot(V2_filtered, label='V2')
plt.title('Filtered V1 and V2')
plt.xlabel('Sample Number')
plt.ylabel('Value')
plt.legend()


plt.tight_layout()

plt.savefig('second_run/output_figure.png', dpi=300)  # saves the figure as a PNG with a resolution of 300 dpi
plt.show()
