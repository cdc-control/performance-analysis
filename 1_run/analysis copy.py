import numpy as np
import matplotlib.pyplot as plt


def remove_outliers(data):
  Q1 = np.percentile(data, 25)
  Q3 = np.percentile(data, 75)
  IQR = Q3 - Q1
  lower_bound = Q1 - 1.5 * IQR
  upper_bound = Q3 + 1.5 * IQR
  return data[(data >= lower_bound) & (data <= upper_bound)]


# Step 0: Load Data
with open('first_run/tstCDC_2023_09_05-18_11_28.txt', 'r', encoding="utf-8" ) as f:
  data_str = f.read()

data = np.array([list(map(float, line.split())) for line in data_str.split('\n') if line])
V1, V2, R = data[:,0], data[:,1], data[:,2]

V1_offset = (V1[::2] + V1[1::2]) / 2  
V2_offset = (V2[::2] + V2[1::2]) / 2

V1_no_offset = V1 - np.repeat(V1_offset, 2)
V2_no_offset = V2 - np.repeat(V2_offset, 2)

V_final = np.absolute( (V1_no_offset + V2_no_offset) / 2)
         

# Step 2: Convert Resistance to Temperature (simplified for example)
Temperature = (R - 100) / 0.385  # A simple linear relationship for PT100 between 0°C and 100°C



# Compute the cross-correlation
cross_corr = np.correlate(V_final, Temperature, mode='full')

# Create an array for lags
lags = np.arange(-len(V_final) + 1, len(V_final))

# Convert V_final to ppm
V_final_ppm = (V_final - 10) * 1e6 / 10

V_final_ppm_filtered = remove_outliers(V_final_ppm)


# Step 4: Plotting
plt.figure(figsize=(12, 8))


plt.subplot(2, 2, 1)
plt.plot(V_final)

plt.subplot(2, 2, 2)
plt.plot(Temperature)

plt.subplot(2, 2, 3)
plt.plot(V_final_ppm_filtered)

plt.subplot(2, 2, 4)

# Find the peak of the cross-correlation
peak_lag = lags[np.argmax(np.abs(cross_corr))]  # We use np.abs to consider negative peaks as well
peak_value = cross_corr[np.argmax(np.abs(cross_corr))]

plt.plot(lags, cross_corr)

plt.scatter(peak_lag, peak_value, color='red')  # Highlighting the peak
plt.text(peak_lag, peak_value, f'Peak: ({peak_lag}, {peak_value:.2f})', 
         verticalalignment='bottom', horizontalalignment='right', color='red')  # Annotating the peak

plt.title('Cross-correlation between V_final and Temperature')
plt.xlabel('Lag')
plt.ylabel('Cross-correlation')
plt.grid(True)
plt.tight_layout()
plt.show()



plt.tight_layout()
plt.show()
