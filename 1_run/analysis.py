import numpy as np
import matplotlib.pyplot as plt


def remove_outliers(data):
  Q1 = np.percentile(data, 25)
  Q3 = np.percentile(data, 75)
  IQR = Q3 - Q1
  lower_bound = Q1 - 1.5 * IQR
  upper_bound = Q3 + 1.5 * IQR
  return data[(data >= lower_bound) & (data <= upper_bound)]


# Step 0: Load Data
with open('first_run/tstCDC_2023_09_05-18_11_28.txt', 'r', encoding="utf-8" ) as f:
  data_str = f.read()

data = np.array([list(map(float, line.split())) for line in data_str.split('\n') if line])
V1, V2, R = data[:,0], data[:,1], data[:,2]

# Step 1: Data Preparation
V_avg = (V1 + V2) / 2
V_offset = (V_avg[::2] + V_avg[1::2]) / 2
V_final = V_avg - np.repeat(V_offset, 2)
V_final = V_final[1::2]  # Keep every other row

# Step 2: Convert Resistance to Temperature (simplified for example)
Temperature = (R - 100) * 0.385  # A simple linear relationship for PT100 between 0°C and 100°C


# Convert V_final to ppm
V_final_ppm = (V_final - 10) * 1e6 / 10

V_final_ppm_filtered = remove_outliers(V_final_ppm)


# Step 4: Plotting
plt.figure(figsize=(12, 8))

plt.subplot(2, 2, 1)
plt.plot(V_final_ppm)
plt.ylabel("ppm of 10V")
plt.title("Reconstructed Voltage in ppm vs Time")

plt.subplot(2, 2, 2)
plt.plot(Temperature)
plt.title("Temperature vs Time")

plt.subplot(2, 2, 3)
plt.plot(V_final_ppm_filtered)
plt.title("Reconstructed Voltage vs Temperature (outliers removed)")

plt.subplot(2, 2, 4)
plt.scatter(Temperature[::2], V_final)
plt.title("Reconstructed Voltage vs Temperature")

plt.tight_layout()
plt.show()
