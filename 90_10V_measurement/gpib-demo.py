import serial

class GPIBController:
  def __init__(self, port, baud_rate=9600, timeout=0.5):
    self.ser = serial.Serial(port, baud_rate, timeout=timeout)

  def send_command(self, command):
    try:
      self.ser.write((command + '\n').encode())
      response = self.ser.read(256)
      return response
    except serial.SerialException as e:
      print(f"Serial error: {e}")
      return None

  def set_mode(self, mode):
    command = f"++mode {mode}"
    return self.send_command(command)

  def set_address(self, address):
    command = f"++addr {address}"
    return self.send_command(command)

  def set_auto_mode(self, auto_mode):
    command = f"++auto {auto_mode}"
    return self.send_command(command)

  def custom_plot(self):
    command = "plot;"
    return self.send_command(command)

  def close(self):
    self.ser.close()

if __name__ == '__main__':
  try:
    gpib = GPIBController('/dev/ttyUSB0')
    
    addr = 20
    gpib.set_mode(1)
    gpib.set_address(addr)
    gpib.set_auto_mode(1)
    
    print("Sending custom plot command:")
    response = gpib.custom_plot()
    if response:
      print("Response:", response)

  except KeyboardInterrupt:
    gpib.close()
  except Exception as e:
    print(f"Error: {e}")
  finally:
    gpib.close()
